import './App.scss';
import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './components/Home';
import Packages from './components/Packages';
import Team from './components/Team';
import AboutUs from './components/AboutUs';
import Clients from './components/Clients';
import Cars from './components/Cars';
import NileCruises from './components/NileCruises';
import Thanks from './components/Thanks';
import BridgesPointsCard from './components/BridgesPointsCard';
import HajjAndOmra from './components/HajjAndOmra';
import Landing from './components/Landing';

function App() {
  return (
    <div className="App">
      <div style={{ width: '128px', height: '80px', position: 'fixed', zIndex: '232323', top: '280px', right: '0px', backgroundColor: '#1bb1dc' }}>

        <div className="col-md-12 text-center" style={{ color: '#fff', padding: '0px', paddingTop: '7px', fontSize: '13px' }}>
          <strong>Call us on <br /> 16354 <br /></strong>

          <div style={{ width: '40px', marginTop: '2px', float: 'right', marginRight: '20px' }}>
            <a href="https://www.facebook.com/roccatraveleg/" target="_blank" style={{ padding: '0px' }}>
              <i className="fa fa-facebook fa-2x text-white-50" aria-hidden="true"></i>
            </a>
          </div>
          <div style={{ width: '40px', marginTop: '2px', float: 'right' }}>
            <a href="https://twitter.com/roccatraveleg?s=09" target="_blank" style={{ padding: '0px' }}>
              <i className="fa fa-twitter fa-2x text-white-50" aria-hidden="true"></i>
            </a>
          </div>
        </div>
      </div>

      <Router>
        <Header />

        <Route exact path="/" component={Home}></Route>
        <Route path="/about-us" component={AboutUs}></Route>
        <Route path="/packages" component={Packages}></Route>
        <Route path="/team" component={Team}></Route>
        <Route path="/clients" component={Clients}></Route>
        <Route path="/cars" component={Cars}></Route>
        <Route path="/nile-cruises" component={NileCruises}></Route>
        <Route path="/thanks" component={Thanks}></Route>
        <Route path="/bridges-points-card" component={BridgesPointsCard}></Route>
        <Route path="/hajj-omra" component={HajjAndOmra}></Route>
        <Route path="/landing" component={Landing}></Route>

        <Footer />
      </Router>

      {/* < !--    < a href = "#" className="back-to-top" > <i className="fa fa-chevron-up"></i></a > -->
    < !--Uncomment below i you want to use a preloader-- >
    < !-- < div id = "preloader" ></div > --> */}
    </div>
  );
}

export default App;
