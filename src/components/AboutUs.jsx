import React from 'react';

function AboutUs() {
  return (
    <main>
      <section id="background" style={{ marginTop: '100px' }}>
        <div class="container">
          <header className="section-header">
            <h3>Chronological background</h3>
          </header>

          <div className="row justify-content-center">
            <div className="col-lg-7 col-md-6">
              <div className="background-content">
                <ul>
                  <li className="my-2">
                    The Company is a tour and travel service company established in1985 after acquiring the license (number541) It started operating as a limited liability company out of one small office in Alexandria with three employees.
                </li>
                  <li className="my-2">
                    In 2008 the company’s legal  was changed to a joint-Group company and the management under took expansion efforts by establishing in Downtown,  kasrelnilen ,sheikzayed, representative offices in KSA .the past two  decades Rocco Group has established a proven track record and a reputable name in the market as one of the top 50 travel agents
                </li>
                  <li className="my-2">
                    And participation in some companies
                    Bridges, Jusur ,Maraheb , umrahHolidays , Newsamart , ajwa, AYLIN & passinelle
                </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-5 col-md-6">
              <div className="background-img">
                <img src="/img/about-1.jpg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="about" className="pt-0">
        <div className="container">
          <header className="section-header">
            <h3>Business lines</h3>
          </header>

          <h5 className="text-center">Rocca Group operates three main lines of business:</h5>
          <div className="row">
            <div className="col-lg-7 col-md-6">
              <div className="about-content">
                <ul>
                  <li>
                    <i className="ion-android-checkmark-circle"></i>
                    <strong>Ticketing</strong>
                    <ol className="ml-3">
                      <li>Main Ticketing (Covering all international airlines)</li>
                      <li>PSA Ticketing (Romanian Airline“TAROM” & Salam Air)</li>
                    </ol>
                  </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i>
                    <strong>System on GSA Online</strong>
                    <ol className="ml-3">
                      <li>eRehla B2C</li>
                      <li>Traviology B2B</li>
                      <li>TKT2HTL</li>
                      <li>UmrahHolidays</li>
                      <li>Maraheb Hajj & omra</li>
                      <li>Bridges</li>
                      <li>Golden Sky</li>
                    </ol>
                  </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i>
                    <strong>Incoming Tourism</strong>
                    <ol className="ml-3">
                      <li>GSA Aylin Sarajevo</li>
                      <li>Outgoing Tourism</li>
                      <li>MICE ( Meetings, Incentives, Conferences & Events</li>
                      <li>Religious Tourism (Haj& Omra)</li>
                    </ol>
                  </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i>
                    <strong>Car rental</strong>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-5 col-md-6 d-flex flex-column justify-content-center">
              <div className="about-img">
                <img src="/img/about-2.jpg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="background" className="pt-0">
        <div class="container">
          <header className="section-header">
            <h3>Business strategy</h3>
          </header>

          <div className="row justify-content-center">
            <div className="col-lg-5 col-md-6">
              <div className="background-img">
                <img src="/img/travel.jpg" alt="" />
              </div>
            </div>
            <div className="col-lg-7 col-md-6">
              <div className="background-content">
                <h5>
                  The Company service market in Egypt is highly fragmented with numerous players varying in size from large to micro companies offering different various travel services. In the aim of positioning itself in the market Rocco follows a business strategy that focuses on
                </h5>
                <ol>
                  <li className="my-2">
                    Identifying competitive advantages as a key business focus; quick response to market changes and effective risk mitigation.
                </li>
                  <li className="my-2">
                    Allying with reputable and reliable partners worldwide.
                </li>
                  <li className="my-2">
                    Promoting a customer-driven approach among the staff to attract and retain clients and hiring and retaining experienced employees with a strong knowledge of the travel business.
                  </li>
                </ol>
              </div>
            </div>
          </div>

          <hr />
          <div className="row justify-content-center">
            <p>Compant License number: 541/1985</p>
          </div>
        </div>
      </section>
    </main>
  )
}

export default AboutUs;