import React, { Component } from 'react';

export default class BookToFly extends Component {
  render() {
    return (
      <div className="p-3 booking-form mt-5">
        <form method="POST" action="https://rocca-travel.com/rocca-mail/api/book.php">
          <legend className="text-center text-black-50"><i className="fa fa-plane"></i> Book To fly</legend>
          <div className="form-group">
            <span className="form-label">Your Destination</span>
            <input className="form-control" name="destination" type="text" placeholder="Enter a destination or hotel name" required></input>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <span className="form-label">Check In</span>
                <input className="form-control" name="checkIn" type="date" required></input>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group">
                <span className="form-label">Check out</span>
                <input className="form-control" name="checkOut" type="date" required></input>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-4">
              <div className="form-group">
                <span className="form-label">Trip</span>
                <select className="form-control" name="trip">
                  <option value="one way">One way</option>
                  <option value="round trip">Round trip</option>
                </select>
                <span className="select-arrow"></span>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="form-group">
                <span className="form-label">Adults</span>
                <select className="form-control" name="adults">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
                <span className="select-arrow"></span>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="form-group">
                <span className="form-label">Children</span>
                <select className="form-control" name="children">
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
                <span className="select-arrow"></span>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-6">
              <div className="form-group">
                <span className="form-label">Name</span>
                <input className="form-control" type="text" name="name" required></input>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="form-group">
                <span className="form-label">Phone number</span>
                <input className="form-control" type="text" name="phoneNumber" required></input>
              </div>
            </div>
          </div>
          <div className="form-btn d-flex justify-content-center">
            <button className="btn btn-outline-info" type="submit">
              <i className="fa fa-check-circle mx-1" aria-hidden="true"></i>
              Send your request
            </button>
          </div>
        </form>
      </div>
    )
  }
}
