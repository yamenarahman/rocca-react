import React from 'react';
import RegisterForCard from "./RegisterForCard";

function BridgesPointsCard() {
  return (
    <section id="team" className="section-bg bg-white" style={{marginTop: '100px'}}>
      <div className="container">
        <div className="section-header">
          <h3><i class="fa fa-id-card" aria-hidden="true"></i> Bridges points card</h3>
        </div>

        <div className="row justify-content-center">
          <div className="col-12 wow fadeInUp">
            <div className="member">
              <img src="/img/bridges-card.jpeg" className="img-fluid" alt=""/>
              <div className="member-info">
                <div className="member-info-content">
                  <h4>
                    The chart below illustrates the Status Miles required to qualify for or to
                    retain a Silver Tier, a Gold Tier and a RED Tier.
                  </h4>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <table className="table">
              <thead className="thead-light">
                <tr>
                  <th rowSpan={2}>Card Type</th>
                  <th rowSpan={2}>Validity (from date of issue)</th>
                  <th colSpan={2}>Status a file / Flights Requirement</th>
                </tr>
                <tr>
                  <th>Qualifying Criteria</th>
                  <th>Retention Criteria</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Silver Card</td>
                  <td>One Calendar Year</td>
                  <td>30 Night / 20 flights</td>
                  <td>Not Applicable</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <table className="table">
              <thead>
                <tr>
                  <th>Privilege</th>
                  <th>Silver</th>
                  <th>Gold</th>
                  <th>Red</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Dedicated phone line</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>Yes</td>
                </tr>
                <tr>
                  <td>Priority waitlist</td>
                  <td>No</td>
                  <td>Yes</td>
                  <td>Yes</td>
                </tr>
                <tr>
                  <td colSpan={4}>
                    <b>Priority waitlist </b>
                    When seats are unavailable on your preferred flight, you will be given priority on the waiting list.
                    RED members receive top, overall priority.
                  </td>
                </tr>
                <tr>
                  <td>Guaranteed seat in Economy Class</td>
                  <td>No</td>
                  <td>No</td>
                  <td>Yes</td>
                </tr>
                <tr>
                  <td colSpan={4}>
                    <b>Guaranteed seat in Economy Class </b>
                    Your RED membership entitles you to a guaranteed seat in Economy Class for bookings made within 48
                    hours of departure. Conditions apply.
                  </td>
                </tr>
                <tr>
                  <td>Special discount</td>
                  <td>3%</td>
                  <td>6%</td>
                  <td>8%</td>
                </tr>
                <tr>
                  <td>In- The yacht discount</td>
                  <td>5%</td>
                  <td>7%</td>
                  <td>10%</td>
                </tr>
                <tr>
                  <td colSpan={4}>
                    <b>In- The yacht discount </b>
                    You are entitled to special discounts on in-the Yacht. silver members receive a discount of 5%, Gold
                    members receive a discount of 7% and RED members receive a discount of 10%.*
                    *Discount cannot be used in conjunction with any other promotion and can only be availed only on
                    presenting your permanent plastic membership card
                  </td>
                </tr>
                <tr>
                  <td>In- Nile cruises Rocca discount</td>
                  <td>7%</td>
                  <td>10%</td>
                  <td>15%</td>
                </tr>
                <tr>
                  <td colSpan={4}>
                    <b>In- Nile cruises Rocca discount </b>
                    You are entitled to special discounts on in-Nile cruises Rocca . silver members receive a discount
                    of 7%, Gold members receive a discount of 10% and RED members receive a discount of 15%.*
                    *Discount cannot be used in conjunction with any other promotion and can only be availed only on
                    presenting your permanent plastic membership card.
                  </td>
                </tr>
                <tr>
                  <td>Meet and assist</td>
                  <td>Yes</td>
                  <td>Yes</td>
                  <td>Yes</td>
                </tr>
                <tr>
                  <td colSpan={4}>
                    <b>Meet and assist </b>
                    When requested at least 48 hours prior to travelling, you will be met at
                    ssh,HRG,RMF,SHL,CAI,ALX,LXR& ASW Airport by a member of Rocca’s staff and assisted Trips Aldkhalih
                  </td>
                </tr>
                <tr>
                  <td>Upgrade vouchers</td>
                  <td>No</td>
                  <td>No</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td colSpan={4}>
                    <b>Upgrade vouchers </b>
                    Each year, upon receiving or renewing your Gold membership, you will receive two free upgrade
                    vouchers. These vouchers can be used by you or an accompanying traveller. Upgrades should be
                    requested through the Membership Service Centre and are subject to the regular booking rules.
                  </td>
                </tr>
                <tr>
                  <td>Redemption Upgrade from ROOM Class to First Class</td>
                  <td>No</td>
                  <td>No</td>
                  <td>Yes</td>
                </tr>
                <tr>
                  <td colSpan={4}>
                    <b>Redemption Upgrade from ROOM Class to First Class </b>
                    Bridges RED Members can request for redemption upgrades from
                  </td>
                </tr>
                <tr>
                  <td>Rerouting / Rebooking deadline and service charges:</td>
                  <td>2 day before departure</td>
                  <td>2 day before departure</td>
                  <td>3 day before departure</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            <RegisterForCard />
          </div>
        </div>

      </div>
    </section>
  );
}

export default BridgesPointsCard;
