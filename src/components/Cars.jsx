import React from 'react'

function Cars() {
  return (
    <section id="team" className="section-bg bg-white" style={{ marginTop: '100px' }}>
      <div className="container">
        <div className="section-header">
          <h3>Buses</h3>
        </div>

        <div className="row justify-content-center">
          <div className="col-lg-3 col-md-6 wow fadeInUp">
            <div className="member">
              <img src="/img/mercedes-600.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Mercedes 600</h4>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div className="member">
              <img src="/img/h1-bus.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Hyundai H1</h4>
                </div>
              </div>
            </div>
          </div>
        </div>

        <hr />

        <div className="section-header">
          <h3>Cars</h3>
        </div>

        <div className="row justify-content-center">
          <div className="col-lg-3 col-md-6 wow fadeInUp">
            <div className="member">
              <img src="/img/car-1.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Mercedes e200</h4>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div className="member">
              <img src="/img/car-2.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Tusan</h4>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div className="member">
              <img src="/img/car-3.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Elantra AD</h4>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div className="member">
              <img src="/img/car-4.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Elantra HD</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Cars;
