import React from 'react'

function Clients() {
  return (
    <main className="container">
      <section id="background" style={{ marginTop: '100px' }}>
        <header className="section-header">
          <h3>Corporate clients</h3>
        </header>

        <div className="row justify-content-center">
          <div className="col-md-5">
            <ul className="list-unstyled text-center">
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Energya Cables - El Sewedy Helal
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Energya Power Cables
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Wadi El Nile Cement
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  CIB Bank
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  AKAM Company
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Banque Misr
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Royal Mark Company
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Memaar Al Morshedy
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  El Salab
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Rashdan Group
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Ghabbour Auto
                </h6>
              </li>
            </ul>
          </div>
          <div className="col-md-5">
            <ul className="list-unstyled text-center">
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Al Ahly for Real Estate Development
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  United bank
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Egyptian shooting club
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Electricity company
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Sporting club
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Passinelle Company
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Arab academy
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Ofok
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Wadi group
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Rev it up
                </h6>
              </li>
              <li>
                <h6>
                  <i class="fa fa-handshake-o text-primary mr-2" aria-hidden="true"></i>
                  Abaza auto
                </h6>
              </li>
            </ul>
          </div>
        </div>
      </section>
    </main>
  )
}

export default Clients;
