import React from 'react';

function Footer() {
  return (
    <footer id="footer" className="section-bg">
      <div className="footer-top">
        <div className="container">
          <header className="section-header">
            <h3>Contact us</h3>
          </header>

          <div className="row">
            <div className="col-12">
              <div className="row">
                <div className="col-md-3 col-sm-12">
                  <div className="footer-info text-center">
                    <div className="d-flex justify-content-center">
                      <img
                        src="/img/rocca-travel.png"
                        className="img-fluid mb-3"
                        style={{ height: '200px' }}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-md-3 col-sm-12">
                  <div className="footer-links text-center">
                    <h4>Giza</h4>
                    <p>
                      22B, Rawdat Zayed,
                      El Sheikh Zayed, Giza, Egypt <br />
                      <strong>Email:</strong> info@rocca-travel.com<br />
                    </p>
                  </div>
                  <div className="social-links text-center">
                    <a href="#" className="twitter"
                    ><i className="fa fa-twitter"></i
                    ></a>
                    <a href="#" className="facebook"
                    ><i className="fa fa-facebook"></i
                    ></a>
                    <a href="#" className="instagram"
                    ><i className="fa fa-instagram"></i
                    ></a>
                    <a href="#" className="linkedin"
                    ><i className="fa fa-linkedin"></i
                    ></a>
                  </div>
                </div>
                <div className="col-md-3 col-sm-12">
                  <div className="footer-links text-center">
                    <h4>Cairo</h4>
                    <p>
                      33 Kasr El Nile St.
                      Downtown, Cairo, Egypt <br />
                      <strong>Email:</strong> info@rocca-travel.com<br />
                    </p>
                  </div>
                  <div className="social-links text-center">
                    <a href="#" className="twitter"
                    ><i className="fa fa-twitter"></i
                    ></a>
                    <a href="#" className="facebook"
                    ><i className="fa fa-facebook"></i
                    ></a>
                    <a href="#" className="instagram"
                    ><i className="fa fa-instagram"></i
                    ></a>
                    <a href="#" className="linkedin"
                    ><i className="fa fa-linkedin"></i
                    ></a>
                  </div>
                </div>
                <div className="col-md-3 col-sm-12">
                  <div className="footer-links text-center">
                    <h4>Cairo</h4>
                    <p>
                      24 Mahmoud Bassiouny St.
                      El Tahrir, Cairo, Egypt <br />
                      <strong>Email:</strong> info@rocca-travel.com<br />
                    </p>
                  </div>
                  <div className="social-links text-center">
                    <a href="#" className="twitter"
                    ><i className="fa fa-twitter"></i
                    ></a>
                    <a href="#" className="facebook"
                    ><i className="fa fa-facebook"></i
                    ></a>
                    <a href="#" className="instagram"
                    ><i className="fa fa-instagram"></i
                    ></a>
                    <a href="#" className="linkedin"
                    ><i className="fa fa-linkedin"></i
                    ></a>
                  </div>
                </div>
              </div>
              <div className="row d-flex justify-content-center">
                <div className="col-md-3 col-sm-12">
                  <div className="footer-links text-center">
                    <h4>Alex</h4>
                    <p>
                      29 El Ghorfa El Togareya St.
                      Raml Station, Alexandria, Egypt <br />
                      <strong>Email:</strong> info@rocca-travel.com<br />
                    </p>
                  </div>
                  <div className="social-links text-center">
                    <a href="#" className="twitter"
                    ><i className="fa fa-twitter"></i
                    ></a>
                    <a href="#" className="facebook"
                    ><i className="fa fa-facebook"></i
                    ></a>
                    <a href="#" className="instagram"
                    ><i className="fa fa-instagram"></i
                    ></a>
                    <a href="#" className="linkedin"
                    ><i className="fa fa-linkedin"></i
                    ></a>
                  </div>
                </div>
                <div className="col-md-3 col-sm-12">
                  <div className="footer-links text-center">
                    <h4>Alex</h4>
                    <p>
                      14 El Hedaya Mosque St.
                      Bolkly, Alexandria, Egypt <br />
                      <strong>Email:</strong> info@rocca-travel.com<br />
                    </p>
                  </div>
                  <div className="social-links text-center">
                    <a href="#" className="twitter"
                    ><i className="fa fa-twitter"></i
                    ></a>
                    <a href="#" className="facebook"
                    ><i className="fa fa-facebook"></i
                    ></a>
                    <a href="#" className="instagram"
                    ><i className="fa fa-instagram"></i
                    ></a>
                    <a href="#" className="linkedin"
                    ><i className="fa fa-linkedin"></i
                    ></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="copyright">
          &copy; Copyright <strong>Rocca </strong>. All Rights Reserved
                <i className="fa fa-phone mx-1"></i><b>16354</b>
                <hr/>
                <p class="bold">Company License number : 541/1985</p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;