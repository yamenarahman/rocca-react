import React, { Component } from 'react'

export default class HajjAndOmra extends Component {
  render() {
    return (
      <section id="team" className="section-bg bg-white" style={{ marginTop: '100px' }}>
        <div className="container">
          <div className="section-header">
            <h3>Hajj and Omra</h3>
          </div>

          <div className="row justify-content-center">
            <div className="col-12 wow fadeInUp">
              <div className="member">
                <img src="/img/omra.jpg" className="img-fluid" alt="" />
                <div className="member-info">
                  <div className="">
                    <h3 className="text-white">
                      برامج الحج والعمرة
                    </h3>
                    <h4 className="text-white">
                      اختار بنفسك برنامج عمرتك والأماكن المفضلة وحدد ميزانيتك
ودعنا نكون في خدمتك
المهم سجل بياناتك لان الإعداد محدودة
وسنتواصل معك فورا
                    </h4>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row justify-content-center" style={{ direction: 'rtl' }}>
            <div className="col-12 wow fadeInUp">
              <form method="POST" action="https://rocca-travel.com/rocca-mail/api/omra.php"
                className="bg-light p-3 rounded"
              >
                <legend className="text-center my-5">
                  افضل برامج الحج و العمرة مع روكا للسياحة...
                  برامجنا تناسب كل المستويات لكننا أيضا ممكن من خلال مكاتبنا بمكة والمدينة نكون في خدمتك طوال العام الهجري
من الان وحتي نهاية رمضان
                </legend>

                <h4 class="text-success text-center">بتفكر تعمل عمرة بعد الكورونا؟ املأ الاستمارة لتكون أول من يذهب إلى العمرة</h4>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <span className="form-label pull-right">الاسم</span>
                      <input className="form-control" type="text" name="name" required></input>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <span className="form-label pull-right">رقم الموبايل</span>
                      <input className="form-control" type="text" name="phone" required></input>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <span className="form-label pull-right">رقم الواتساب</span>
                      <input className="form-control" type="text" name="whatsapp" required></input>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <span className="form-label pull-right">في اي الاوقات تفضل السفر الى العمرة ؟</span>
                      <select name="date" className="form-control">
                        <option value="الفترة من محرم (سبتمبر2019) الى ربيع ثاني(ديسمبر)">
                          الفترة من محرم (سبتمبر2020) الى ربيع ثاني(ديسمبر)
                        </option>
                        <option value="الفترة من جمادي اول(يناير2020) حتى شعبان(ابريل)">
                          الفترة من جمادي اول(يناير2020) حتى شعبان(ابريل)
                        </option>
                        <option value="غير ملتزم بوقت محدد">غير ملتزم بوقت محدد</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <span className="form-label pull-right">اي مستوى من البرامج تفضل</span>
                      <select name="class" className="form-control">
                        <option value="٥ نجوم">٥ نجوم</option>
                        <option value="٤ نجوم">٤ نجوم</option>
                        <option value="مستوى اقتصادي">مستوى اقتصادي</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <span className="form-label pull-right">هل تفضل فندق محدد ؟</span>
                      <input className="form-control" type="text" name="preferred_hotel" required
                        placeholder="نعم او لا مع الذكر"
                      >
                      </input>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <span className="form-label pull-right">مدة البرنامج المناسبة؟ </span>
                      <select className="form-control" name="duration" required
                      >
                        <option value="9 ايام / 8 ليالي">9 ايام / 8 ليالي</option>
                        <option value="10 ايام / 9 ليالي">10 ايام / 9 ليالي</option>
                        <option value="أخرى">أخرى</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6">
                    <div className="form-group">
                      <span className="form-label pull-right">قرر ميزانيتك و احنا هنوفرلك الخدمه المناسبه</span>
                      <input className="form-control" type="text" name="budget" required></input>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-12">
                    <div className="form-btn d-flex justify-content-center form-group">
                      <button className="btn btn-success" type="submit">
                        <i className="fa fa-check-circle mx-1" aria-hidden="true"></i>
                        إرسال البيانات
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section >
    )
  }
}
