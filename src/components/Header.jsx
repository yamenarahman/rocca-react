import React, { Component } from 'react'

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mode: 'web'
    };
  }

  mobileNav() {
    document.body.classList.add('mobile-nav-active');

    this.setState({
      mode: 'mobile'
    });
  }

  webNav() {
    document.body.classList.remove('mobile-nav-active');

    this.setState({
      mode: 'web'
    });
  }

  toggleNav() {
    this.state.mode === 'mobile'
      ? this.webNav()
      : this.mobileNav();
  }

  render() {
    return (
      <header id="header">
        <button type="button" className="mobile-nav-toggle d-lg-none" onClick={() => this.toggleNav()}>
          <i className="fa fa-bars"></i>
        </button>

        <div id="topbar">
          <div className="container">
            <div className="social-links">
              <span className="mx-1">
                <span className="flag-icon flag-icon-eg"></span> Egypt
            </span>
              <span className="mx-1">
                <span className="flag-icon flag-icon-sa"></span> Saudi Arabia
            </span>
              <span className="mx-1">
                <span className="flag-icon flag-icon-jo"></span> Jordon
            </span>
              <span className="mx-1">
                <span className="flag-icon flag-icon-ba"></span> Bosnia
            </span>
              <span className="mx-1">
                <span className="flag-icon flag-icon-lb"></span> Lebanon
            </span>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="logo float-left">
            <a className="scrollto" href="/">
              <img src="/img/rocca.png" alt="" className="img-fluid" id="rocca-logo" />
            </a>
          </div>

          <nav className="main-nav float-right d-none d-lg-block">
            <ul>
              <li><a href="/">Home</a></li>
              <li><a href="/packages">Packages & Hotels</a></li>
              <li><a href="/cars">Buses & Cars</a></li>
              <li><a href="/nile-cruises">Nile cruises</a></li>
              <li><a href="/hajj-omra">Hajj & Omra</a></li>
              <li><a href="/bridges-points-card">Bridges points card</a></li>
              <li className="drop-down"><a href="/">Rocca</a>
                <ul>
                  <li><a href="/about-us">About us</a></li>
                  <li><a href="/team">Team</a></li>
                  <li><a href="/clients">Corporate clients</a></li>
                  <li><a href="/">International offices</a></li>
                </ul>
              </li>
            </ul>
          </nav>

          <nav className="mobile-nav d-lg-none">
            <ul>
              <li><a href="/">Home</a></li>
              <li><a href="/packages">Packages & Hotels</a></li>
              <li><a href="/cars">Buses & Cars</a></li>
              <li><a href="/nile-cruises">Nile cruises</a></li>
              <li><a href="/hajj-omra">Hajj & Omra</a></li>
              <li><a href="/bridges-points-card">Bridges points card</a></li>
              <li><a href="/about-us">About us</a></li>
              <li><a href="/team">Team</a></li>
              <li><a href="/clients">Corporate clients</a></li>
              <li><a href="/">International offices</a></li>
            </ul>
          </nav>
        </div>
      </header>
    )
  }
}