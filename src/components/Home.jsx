import React from 'react';
import Intro from './Intro';

function Home() {
  return (
    <main id="main">
      <Intro />
      <section id="about">
        <div className="container">
          <div className="row">
            <div className="col-lg-5 col-md-6">
              <div className="about-img">
                <img src="/img/travel.jpg" alt="" />
              </div>
            </div>

            <div className="col-lg-7 col-md-6">
              <div className="about-content">
                <h2>Who is Rocca group?</h2>
                <p>
                  The <b>Rocca</b> is the outgrowth of so many years in
                                <b>Tourism</b>, invested in presenting you a complete tourist
package, an animated album where you can walk through its
folios.
                            </p>
                            <p class="bold">Company License #: 541/1985</p>
                <p>
                  Through the integration of important assets embracing
                  different sectors of the tourism industry and with an
                                extensive business dimensions the <b>Rocca Group</b> was
created as the matrix company for variety of companies each
producing its specific line of expertise and providing the
premium service its span.
                            </p>
              </div>
            </div>
          </div>

          <h3 className="my-5">Rocco tours offers a variety of travel and destination management services that include:</h3>

          <div className="row">
            <div className="col-lg-7 col-md-6">
              <div className="about-content">
                <ul>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Domestic and
                    international ticketing.
                  </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Corprate rates
                    and corprate travel expense analysis and management.
                                </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Meet and
                    assist, transportation and group arrangements.
                                </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Hotel bookings
                    locally and abroad.
                                </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Nile cruise
                    bookings.
                                </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Hajj & Omra.
                                </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Charter
                    organizing.
                                </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Ticket
                    delivery.
                                </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> 24/7 toll-free
                    help lines.
                                </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Conference and
                    convention organizing.
                                </li>
                  <li>
                    <i className="ion-android-checkmark-circle"></i> Transport and
                    car rental services.
                                </li>
                </ul>
              </div>
            </div>

            <div className="col-lg-5 col-md-6">
              <div className="about-img">
                <img src="/img/travel-2.jpg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}

export default Home;