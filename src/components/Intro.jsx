import React from 'react';
import BookToFly from './BookToFly';

function Intro() {
  return (
    // <Filter />
    <section id="intro" className="clearfix">
      <div className="container d-flex h-100">
        <div className="row justify-content-center align-self-center">
          <div className="col-12">
            {/* <!-- <h2 className="mt-5">
                Travel to Live,<br />Occupy the <span>world!</span>
              </h2> --> */}
            <BookToFly />

            {/* <!-- <a href="#about" className="btn-get-started scrollto">Get Started</!--> --> */}
          </div>

          {/* <div className="col-md-6 intro-img order-md-last order-first d-flex"> */}
          {/* <!-- <img src="/img/travel.svg" alt="" className="img-fluid" width="500px" /> --> */}
          {/* </div> */}
        </div>
      </div>
    </section>
  );
}

export default Intro;