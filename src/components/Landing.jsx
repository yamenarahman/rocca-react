import React, { Component } from 'react'

export default class Landing extends Component {
  render() {
    return (
      <section id="intro" className="clearfix">
        <div className="container h-100">
          <div className="row justify-content-center align-self-center">
            <div className="col-md-8">
              <div className="p-3 booking-form" style={{ marginTop: '12rem' }}>
                <form method="POST" action="https://rocca-travel.com/rocca-mail/api/landing.php">
                  <legend className="text-center">
                    راسلنا
                    <i class="fa fa-share mx-2" aria-hidden="true"></i>
                  </legend>

                  <div className="row">
                    <div className="col-12">
                      <div className="form-group">
                        <span className="form-label pull-right">الاسم</span>
                        <input className="form-control" type="text" name="name" required></input>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12">
                      <div className="form-group">
                        <span className="form-label pull-right">رقم الموبايل</span>
                        <input className="form-control" type="text" name="phone" required></input>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12">
                      <div className="form-group">
                        <span className="form-label pull-right">رقم الواتساب</span>
                        <input className="form-control" type="text" name="whatsapp" required></input>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-12">
                      <div className="form-group">
                        <span className="form-label pull-right">رسالتك</span>
                        <textarea className="form-control" name="message" required></textarea>
                      </div>
                    </div>
                  </div>

                  <div className="form-btn d-flex justify-content-center">
                    <button className="btn btn-outline-success" type="submit">
                      <i className="fa fa-check-circle mx-1" aria-hidden="true"></i>
                      ارسل
                      </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
