import React from 'react';

function NileCruises() {
  return (
    <section id="team" className="section-bg bg-white" style={{ marginTop: '100px' }}>
      <div className="container">
        <div className="section-header">
          <h3>Nile Cruises</h3>
        </div>

        <div className="row justify-content-center">
          <div className="col-lg-3 col-md-6 wow fadeInUp">
            <div className="member">
              <img src="/img/stephanie.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Stephanie</h4>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div className="member">
              <img src="/img/monica-nile-cruise.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Monica</h4>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div className="member">
              <img src="/img/Kasr-el-nile.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Kasr El-nile</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default NileCruises;
