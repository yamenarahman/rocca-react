import React from 'react';

function Packages() {
  return (
    <section id="packages" className="section-bg bg-white" style={{ marginTop: '100px' }}>
      <div className="container">
        <header className="section-header">
          <h3>Packages</h3>
        </header>

        <div className="row justify-content-center">
          <div className="col-md-6 col-lg-4 my-2 wow bounceInUp" data-wow-duration="1.4s">
            <div className="box h-100">
              <a href="/packages" data-toggle="modal" data-target="#casablanca">
                <div className="icon" style={{ backgroundColor: '#fceef3' }}>
                  <i className="ion-plane" style={{ color: '#ff689b' }}></i>
                </div>
                <h4 className="title">
                  Casablanca
                </h4>
                <p className="description">
                  <ul className="list-unstyled">
                    <li className="text-dark text-center">
                      09 AUG 2019 <b>till</b> 16 AUG 2019
                                    </li>
                    <li className="text-dark text-center">
                      12 AUG 2019 <b>till</b> 19 AUG 2019
                                    </li>
                  </ul>
                  <h6 className="text-center">More details</h6>
                </p>
              </a>
            </div>
          </div>

          {/* <!-- Modal --> */}
          <div className="modal fade" id="casablanca" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5>Casablanca</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <p className="description mb-3">
                    <div className="table-responsive">
                      <table className="table table-striped">
                        <thead>
                          <th>Hotel name</th>
                          <th>Check in</th>
                          <th>Check out</th>
                          <th>Meal</th>
                          <th>Prices over one person</th>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              Ibis Moussafir Casablanca city center
                                                        <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                            </td>
                            <td>12 AUG 2019</td>
                            <td>19 AUG 2019</td>
                            <td>Breakfast</td>
                            <td>12,600 EGP</td>
                          </tr>
                          <tr>
                            <td>
                              Onomo Casablanca city center
                                                        <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                            </td>
                            <td>12 AUG 2019</td>
                            <td>19 AUG 2019</td>
                            <td>Breakfast</td>
                            <td>13,250 EGP</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </p>
                  <h4 className="title">Package includes</h4>
                  <p className="description mb-3">
                    <ul className="list-unstyled">
                      <li>
                        <i className="fa fa-check-circle text-success"></i>
                        Roundtrip economy airfare on Egypt Air regular flights, including taxes From (Cairo to)
                                            <b>Note</b> that this time of flight upon availability
                                        </li>
                      <li>
                        <i className="fa fa-check-circle text-success"></i>
                        Accommodation for 7 nights/ 8 days at Hotel of your choice on Bed and Breakfast basis
                                        </li>
                    </ul>
                  </p>
                  <h4 className="title">Package excludes</h4>
                  <p className="description mb-3">
                    <ul className="list-unstyled">
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Any additional item not mentioned in above package
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Personal consumption such as beverages, room service, laundry, telephone calls, etc.
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Visa Fees 1900 EGP ( (2) new personal photos with white background+Passport valid for 6 months+ HR Letter or Vaild commercial registry &
                        valid and up dated tax card a proof of yaerly renewal should be submitted+ Air Ticket + Hotel Booking+ Original stamped bank Statement
                        for last 6 months)
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Any frequent changes in currency Prices
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        <b>Note:</b> Commission to travel agency 250 EGP Per Person
                                        </li>
                    </ul>
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-6 col-lg-4 my-2 wow bounceInUp" data-wow-duration="1.4s">
            <div className="box h-100">
              <a href="/packages" data-toggle="modal" data-target="#kuala">
                <div className="icon" style={{ background: '#fff0da' }}>
                  <i className="fa fa-globe" style={{ color: '#e98e06' }}></i>
                </div>
                <h4 className="title">
                  Kuala lumpur & Bali
                                </h4>
                <p className="description">
                  <ul className="list-unstyled">
                    <li className="text-dark text-center">
                      09 AUG 2019 <b>till</b> 16 AUG 2019
                                    </li>
                    <li className="text-dark text-center">
                      12 AUG 2019 <b>till</b> 19 AUG 2019
                                    </li>
                  </ul>
                  <h6 className="text-center">More details</h6>
                </p>
              </a>
            </div>
          </div>

          <div className="modal fade" id="kuala">
            <div className="modal-dialog modal-lg">
              <div className="modal-content">
                <div className="modal-header">
                  <h4 className="modal-title">Kuala lumpur & Bali</h4>
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div className="modal-body">
                  <p className="description mb-3">
                    <div className="table-responsive">
                      <table className="table table-striped">
                        <thead>
                          <th>Hotel name</th>
                          <th>Check in</th>
                          <th>Check out</th>
                          <th>Meal</th>
                          <th>Prices over one person</th>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              Ubud Village Hotel Bali
                                                        <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                            </td>
                            <td>8 AUG 2019</td>
                            <td>13 AUG 2019</td>
                            <td>Breakfast</td>
                            <td>8,550 EGP</td>
                          </tr>
                          <tr>
                            <td>
                              Sunway Putra Hotel KUL
                                                        <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                            </td>
                            <td>13 AUG 2019</td>
                            <td>15 AUG 2019</td>
                            <td>Breakfast</td>
                            <td>2,520 EGP</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </p>

                  <h4 className="title">Land arrangement</h4>
                  <p className="description mb-3">
                    <ul className="list-group">
                      <li className="list-group-item">
                        DPS Airport / DPS Hotel / DPS Airport <b>1190</b>
                      </li>
                      <li className="list-group-item">
                        KL Airport / KL Hotel / KL Airport <b>1550</b>
                      </li>
                    </ul>
                  </p>
                  <h4 className="title">Package includes</h4>
                  <p className="description mb-3">
                    <ul className="list-unstyled">
                      <li>
                        <i className="fa fa-check-circle text-success"></i>
                        Accommodation for 7 nights/ 8 days at Hotel of your choice on Bed and Breakfast basis
                                        </li>
                      <li>
                        <i className="fa fa-check-circle text-success"></i>
                        Internal Transportation from / to the airport Bali & Kuala lumpur
                                        </li>
                    </ul>
                  </p>
                  <h4 className="title">Package excludes</h4>
                  <p className="description mb-3">
                    <ul className="list-unstyled">
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Any additional item not mentioned in above package
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Personal consumption such as beverages, room service, laundry, telephone calls, etc.
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        <b>Note:</b> Commission to travel agency 250 EGP Per Person
                                        </li>
                    </ul>
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-6 col-lg-4 my-2 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div className="box h-100">
              <a href="/packages" data-toggle="modal" data-target="#limassol">
                <div className="icon" style={{ background: '#e6fdfc' }}>
                  <i className="fa fa-globe" style={{ color: '#3fcdc7' }}></i>
                </div>
                <h4 className="title">Limassol-Cyprus</h4>
                <p className="description">
                  <ul className="list-unstyled">
                    <li className="text-dark text-center">
                      12 AUG 2019 <b>till</b> 17 AUG 2019
                                    </li>
                  </ul>
                  <h6 className="text-center">More details</h6>
                </p>
              </a>
            </div>
          </div>

          <div className="modal fade" id="limassol">
            <div className="modal-dialog modal-lg">
              <div className="modal-content">
                <div className="modal-header">
                  <h4 className="modal-title">Limassol-Cyprus</h4>
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div className="modal-body">
                  <p className="description mb-3">
                    <div className="table-responsive">
                      <table className="table table-striped">
                        <thead>
                          <th>Hotel name</th>
                          <th>Check in</th>
                          <th>Check out</th>
                          <th>Meal</th>
                          <th>Prices over one person</th>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              Pefkos hotel
                                                        <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                            </td>
                            <td>12 AUG 2019</td>
                            <td>17 AUG 2019</td>
                            <td>Breakfast</td>
                            <td>10,740 EGP</td>
                          </tr>
                          <tr>
                            <td>
                              Ajax hotel
                                                        <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                            </td>
                            <td>12 AUG 2019</td>
                            <td>17 AUG 2019</td>
                            <td>Breakfast</td>
                            <td>11,955 EGP</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </p>

                  <h4 className="title">Package includes</h4>
                  <p className="description mb-3">
                    <ul className="list-unstyled">
                      <li>
                        <i className="fa fa-check-circle text-success"></i>
                        Roundtrip economy airfare on Egypt Air regular flights, including taxes From (Cairo to)
                                            <b>Note</b> that this time of flight upon availability
                                        </li>
                      <li>
                        <i className="fa fa-check-circle text-success"></i>
                        Accommodation for 5 nights/ 6 days at Hotel of your choice on Bed and Breakfast basis
                                        </li>
                    </ul>
                  </p>
                  <h4 className="title">Package excludes</h4>
                  <p className="description mb-3">
                    <ul className="list-unstyled">
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Any additional item not mentioned in above package
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Personal consumption such as beverages, room service, laundry, telephone calls, etc.
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Visa Fees 2500 EGP ( (2) new personal photos with white background+Passport valid for 6 months+ HR Letter or Vaild commercial registry &
                        valid and up dated tax card a proof of yaerly renewal should be submitted+ Air Ticket + Hotel Booking+ Original stamped bank Statement
                        for last 6 months)
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Any frequent changes in currency Prices
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        <b>Note:</b> Commission to travel agency 250 EGP Per Person
                                        </li>
                    </ul>
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-6 col-lg-4 my-2 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div className="box h-100">
              <a href="/packages" data-toggle="modal" data-target="#mykonos">
                <div className="icon" style={{ background: '#e6fdfc' }}>
                  <i className="fa fa-globe" style={{ color: '#3fcdc7' }}></i>
                </div>
                <h4 className="title">Mykonos</h4>
                <p className="description">
                  <ul className="list-unstyled">
                    <li className="text-dark text-center">
                      10 AUG 2019 <b>till</b> 17 AUG 2019
                                    </li>
                  </ul>
                  <h6 className="text-center">More details</h6>
                </p>
              </a>
            </div>
          </div>

          <div className="modal fade" id="mykonos">
            <div className="modal-dialog modal-lg">
              <div className="modal-content">
                <div className="modal-header">
                  <h4 className="modal-title">Mykonos</h4>
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div className="modal-body">
                  <p className="description mb-3">
                    <div className="table-responsive">
                      <table className="table table-striped">
                        <thead>
                          <th>Hotel name</th>
                          <th>Check in</th>
                          <th>Check out</th>
                          <th>Meal</th>
                          <th>Prices over one person</th>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              Makis Place
                                                        <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                            </td>
                            <td>10 AUG 2019</td>
                            <td>17 AUG 2019</td>
                            <td>Breakfast</td>
                            <td>48,122 EGP</td>
                          </tr>
                          <tr>
                            <td>
                              Rhenia Mykonos
                                                        <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                              <i className="fa fa-star text-warning"></i>
                            </td>
                            <td>10 AUG 2019</td>
                            <td>17 AUG 2019</td>
                            <td>Breakfast</td>
                            <td>53,036 EGP</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </p>

                  <h4 className="title">Package includes</h4>
                  <p className="description mb-3">
                    <ul className="list-unstyled">
                      <li>
                        <i className="fa fa-check-circle text-success"></i>
                        Roundtrip economy airfare on Egypt Air regular flights, including taxes From (Cairo to)
                                            <b>Note</b> that this time of flight upon availability
                                        </li>
                      <li>
                        <i className="fa fa-check-circle text-success"></i>
                        Accommodation for 7 nights/ 8 days at Hotel of your choice on Bed and Breakfast basis
                                        </li>
                    </ul>
                  </p>
                  <h4 className="title">Package excludes</h4>
                  <p className="description mb-3">
                    <ul className="list-unstyled">
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Any additional item not mentioned in above package
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Personal consumption such as beverages, room service, laundry, telephone calls, etc.
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Visa Fees 2500 EGP ( (2) new personal photos with white background+Passport valid for 6 months+ HR Letter or Vaild commercial registry &
                        valid and up dated tax card a proof of yaerly renewal should be submitted+ Air Ticket + Hotel Booking+ Original stamped bank Statement
                        for last 6 months)
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        Any frequent changes in currency Prices
                                        </li>
                      <li>
                        <i className="fa fa-times text-danger"></i>
                        <b>Note:</b> Commission to travel agency 250 EGP Per Person
                                        </li>
                    </ul>
                  </p>
                </div>
              </div>
            </div>
          </div>

          {/* <!--<div className="col-md-6 col-lg-4 my-2 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">-->
                    <!--    <div className="box h-100">-->
                    <!--        <div className="icon" style={{background: '#e1eeff'}}>-->
                    <!--            <i className="ion-plane" style={{color: '#2282ff'}}></i>-->
                    <!--        </div>-->
                    <!--        <h4 className="title">-->
                    <!--            <a href="#" data-toggle="modal" data-target="#modelId"-->
                    <!--            >Nemo Enim</a-->
                  <!--            >-->
                    <!--        </h4>-->
                    <!--        <p className="description">-->
                    <!--            At vero eos et accusamus et iusto odio dignissimos ducimus qui-->
                    <!--            blanditiis praesentium voluptatum deleniti atque-->
                    <!--        </p>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <!--<div className="col-md-6 col-lg-4 my-2 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">-->
                    <!--    <div className="box h-100">-->
                    <!--        <div className="icon" style={{background: '#ecebff'}}>-->
                    <!--            <i className="ion-earth" style={{color: '#8660fe'}}></i>-->
                    <!--        </div>-->
                    <!--        <h4 className="title">-->
                    <!--            <a href="#" data-toggle="modal" data-target="#modelId"-->
                    <!--            >Travel insurance</a-->
                <!--            >-->
                    <!--        </h4>-->
                    <!--        <p className="description">-->
                    <!--            Et harum quidem rerum facilis est et expedita distinctio. Nam-->
                    <!--            libero tempore, cum soluta nobis est eligendi-->
                    <!--        </p>-->
                    <!--    </div>-->
                    <!--</div>--> */}
        </div>
      </div>
    </section>

  );
}

export default Packages;