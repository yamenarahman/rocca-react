import React, {Component} from 'react';

export default class RegisterForCard extends Component {
  render() {
    return (
      <div className="my-5 bg-light p-3">
        <form method="POST" action="https://rocca-travel.com/rocca-mail/api/card.php">
          <legend className="text-center text-dark">
            <i class="fa fa-file mr-1" aria-hidden="true"></i>
            Bridges points card application
          </legend>

          <div className="row">
            <div className="form-group col-12">
              <span className="form-label">Full name</span>
              <input className="form-control" name="fullname" type="text" required/>
            </div>
          </div>

          <div className="row">
            <div className="form-group col-md-6">
              <span className="form-label">Address</span>
              <input className="form-control" name="address" type="text" required/>
            </div>

            <div className="form-group col-md-3">
              <span className="form-label">City</span>
              <input className="form-control" name="city" type="text" required/>
            </div>

            <div className="form-group col-md-3">
              <span className="form-label">Country</span>
              <input className="form-control" name="country" type="text" required/>
            </div>
          </div>

          <div className="row">
            <div className="form-group col-md-4">
              <span className="form-label">Email</span>
              <input className="form-control" name="email" type="email" required/>
            </div>

            <div className="form-group col-md-4">
              <span className="form-label">Phone number</span>
              <input className="form-control" name="phone" type="text" required/>
            </div>

            <div className="form-group col-md-4">
              <span className="form-label">Home number</span>
              <input className="form-control" name="home" type="text" required/>
            </div>
          </div>

          <div className="row">
            <div className="form-group col-md-6">
              <span className="form-label">Signature</span>
              <input className="form-control" name="signature" type="text" required/>
            </div>

            <div className="form-group col-md-6">
              <span className="form-label">Signature date</span>
              <input className="form-control" name="date" type="date" required/>
            </div>
          </div>

          <div className="row">
            <div className="form-group col-12">
              <span className="form-label">Bridges card type (membership)</span>
              <select className="form-control" name="membership">
                <option value="silver">Bridges silver</option>
                <option value="gold">Bridges gold</option>
                <option value="red">Bridges red</option>
              </select>
              <span className="select-arrow"></span>
            </div>
          </div>

          <div className="form-btn d-flex justify-content-center">
            <button className="btn btn-outline-success" type="submit">
              <i className="fa fa-check-circle mx-1" aria-hidden="true"></i>
              Apply for membership
            </button>
          </div>
        </form>
      </div>
    )
  }
}
