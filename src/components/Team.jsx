import React from 'react';

function Team() {
  return (
    <section id="team" className="section-bg bg-white" style={{ marginTop: '100px' }}>
      <div className="container">
        <div className="section-header">
          <h3>Team</h3>
          <h5 className="text-center mb-5">
            Rocca’s management has over 35 years of experience in the Egyptian travel service field
          </h5>
        </div>

        <div className="row justify-content-center align-items-center">
          <div className="col-lg-9 col-md-6">
            <div className="background-content">
              <ul>
                <li className="my-2">
                  Mr. Turky has accumulated experience in the air travel and ticketing field since 1984 when he started his career with Rocca as an Chairman , before acquiring Maraheb Group as an Chairman, New smart as an Board Member ,umrahHolidays Board Member &Bridges .
                </li>
                <li className="my-2">
                  He is  an IATA member, a member of the Egyptian Travel Agent Association, a member of the Egyptian Hotels Association and a member of ASTA. Mr. Turky has a bachelor's degree in commerce from Alexandra University.
                </li>
              </ul>
            </div>
          </div>
          <div className="col-lg-3 col-md-6">
            <div className="member">
              <img src="/img/team-1.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Nasser Turky</h4>
                  <span>Chairman</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <hr />

        <div className="row justify-content-center align-items-center">
          <div className="col-lg-3 col-md-6">
            <div className="member">
              <img src="/img/team-6.png" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Rehab Ghazy</h4>
                  <span>CEO - Vice chairman</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-9 col-md-6">
            <div className="background-content">
              <ul>
                <li className="my-2">
                  Mrs. Ghazy has 22 years of experience working in travel companies field & Airlines .started my career with San Giovanni & then Red Sea Alexandria Branch as Branch Manager , Neil Red Sea Greece country Manager ,then Air Cairo GM &Oman Air GM also as Teacher for airlines business.
                </li>
                <li className="my-2">
                  Finally as chief executive officer at Rocco Mrs. Ghazy  has a bachelor’s degree in tourism & Master in attracting tourists from abroad from AASTMT University
                </li>
              </ul>
            </div>
          </div>
        </div>

        <hr />

        <div className="row justify-content-center align-items-center">
          <div className="col-lg-9 col-md-6">
            <div className="background-content">
              <ul>
                <li className="my-2">
                  Mr. khalil has 25 years of experience working in travel companies. He started his career with Rocca and held the position of a retail and development manager until 1996 when he joined Rocca Tours as the Area  manager of the company. He is an IATA member  . Mr. Khalil holds a bachelor’s degree in  commerce from Alexandra University
                </li>
              </ul>
            </div>
          </div>
          <div className="col-lg-3 col-md-6">
            <div className="member">
              <img src="/img/khalil.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Eslam Khalil</h4>
                  <span>Area Manager</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <hr />

        <div className="row justify-content-center mt-5">
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div className="member">
              <img src="/img/rou.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Rou</h4>
                  <span>Tourism manager</span>
                </div>
              </div>
            </div>
          </div>
          {/* <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div className="member">
              <img src="/img/team-3.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Raghda Elmalessy</h4>
                  <span>Sales Manager</span>
                </div>
              </div>
            </div>
          </div> */}
          {/* <div className="col-lg-3 col-md-6 wow fadeInUp">
            <div className="member">
              <img src="/img/blank-user.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Sohair Zaky</h4>
                  <span>Aviation manager</span>
                </div>
              </div>
            </div>
          </div> */}
          <div className="col-lg-3 col-md-6 wow fadeInUp">
            <div className="member">
              <img src="/img/bothina.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Bothina Mahmoud</h4>
                  <span>HRM</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row justify-content-center mt-5">
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div className="member">
              <img src="/img/hadeer.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Hadir Sayed</h4>
                  <span>Domestic operator</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div className="member">
              <img src="/img/blank-user.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Ehab</h4>
                  <span>Accountant</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div className="member">
              <img src="/img/menna.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Menna</h4>
                  <span>Outbound operator</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div className="member">
              <img src="/img/kholoud.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Kholoud Mohamed</h4>
                  <span>Office coordinator</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center mt-5">
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div className="member">
              <img src="/img/akram.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Akram</h4>
                  <span>MKTG</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp">
            <div className="member">
              <img src="/img/team-5.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Hanan Alshehawy</h4>
                  <span>Ticket Officer (Alex)</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div className="member">
              <img src="/img/blank-user.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Ahmed Dayhoum</h4>
                  <span>Transfer</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div className="member">
              <img src="/img/sherif.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Sherif</h4>
                  <span>Hajj & Omra SV</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center mt-5">
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div className="member">
              <img src="/img/eslam-darwish.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Eslam Darwish</h4>
                  <span>Agency SV</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div className="member">
              <img src="/img/team-4.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Mohamed Elshenawy</h4>
                  <span>Accountant</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div className="member">
              <img src="/img/hamdy.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Hamdy</h4>
                  <span>Sales rep.</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp">
            <div className="member">
              <img src="/img/blank-user.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Rana</h4>
                  <span>Sales rep.</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row justify-content-center mt-5">
          <div className="col-lg-3 col-md-6 wow fadeInUp">
            <div className="member">
              <img src="/img/mohamed-adel.jpeg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Mohamed Adel</h4>
                  <span>Account Manager</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp">
            <div className="member">
              <img src="/img/blank-user.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Hanaa</h4>
                  <span>MKTG</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div className="member">
              <img src="/img/blank-user.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Waleed</h4>
                  <span>Ticket officer</span>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div className="member">
              <img src="/img/haitham.jpg" className="img-fluid team-avatar" alt="" />
              <div className="member-info">
                <div className="member-info-content">
                  <h4>Haitham</h4>
                  <span>Accountant</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Team;