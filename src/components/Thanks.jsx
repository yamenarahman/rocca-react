import React, { Component } from 'react'

function Thanks() {
  return (
    <div className="App">
      <div style={{ width: '128px', height: '80px', position: 'fixed', zIndex: '232323', top: '280px', right: '0px', backgroundColor: '#1bb1dc' }}>

        <div className="col-md-12 text-center" style={{ color: '#fff', padding: '0px', paddingTop: '7px', fontSize: '13px' }}>
          <strong>Call us on <br /> 16354 <br /></strong>

          <div style={{ width: '40px', marginTop: '2px', float: 'right', marginRight: '20px' }}>
            <a href="https://www.facebook.com/roccatraveleg/" target="_blank" style={{ padding: '0px' }}>
              <i class="fa fa-facebook fa-2x text-white-50" aria-hidden="true"></i>
            </a>
          </div>
          <div style={{ width: '40px', marginTop: '2px', float: 'right' }}>
            <a href="https://twitter.com/roccatraveleg?s=09" target="_blank" style={{ padding: '0px' }}>
              <i class="fa fa-twitter fa-2x text-white-50" aria-hidden="true"></i>
            </a>
          </div>
        </div>
      </div>
      <main>
        <section id="background" style={{ marginTop: '100px' }}>
          <div class="container">
            <header className="section-header">
              <h3>
                <i class="fa fa-check-circle text-success mr-2" aria-hidden="true"></i>
                Thanks! your request is sent successfully!
                <br />
                We'll get in touch with you soon.
              </h3>

              <h5 className="text-center">
                Working days: Sunday to Thursday (9:00AM - 6:00PM)
              </h5>

              <h3 style={{ direction: 'rtl' }}>
                <i class="fa fa-check-circle text-success ml-2" aria-hidden="true"></i>
                تم استقبال بياناتك بنجاح
                <br />
                سيتم التواصل معك في اول يوم عمل
              </h3>

              <h5 className="text-center">
                علما بأن ايام العمل الاسبوعية من الاحد للخميس من الساعة 9 صباحا و حتى 6 مساء
              </h5>
            </header>
          </div>
        </section>
      </main>
    </div>
  )
}

export default Thanks;